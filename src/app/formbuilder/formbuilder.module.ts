import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormbuilderRoutingModule } from './formbuilder-routing.module';
import { FormbuilderComponent } from './formbuilder.component';
import { ControlsModule } from './controls/controls.module';
import { AssemblerModule } from './assembler/assembler.module';
import { PreviewModule } from './preview/preview.module';
 
@NgModule({
  declarations: [FormbuilderComponent],
  imports: [
    CommonModule,
    FormbuilderRoutingModule, ControlsModule, AssemblerModule,PreviewModule
  ]
})
export class FormbuilderModule { }
 


