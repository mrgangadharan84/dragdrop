import { Component, OnInit } from '@angular/core';
import { CtrlDet } from './CtrlDet';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { PreviewComponent } from './preview/preview.component';
@Component({
  selector: 'app-formbuilder',
  templateUrl: './formbuilder.component.html',
  styleUrls: ['./formbuilder.component.css']
})
export class FormbuilderComponent implements OnInit {

  CtrlDetList =new  Array<CtrlDet>(); 
  constructor(private modalService: NgbModal) { }

  ngOnInit(): void {
  }
  OpenPreview() {

    const modalRef = this.modalService.open(PreviewComponent);
    modalRef.componentInstance.PreviewCtlsDet = this.CtrlDetList;

    
  }
   
}
