import { Directive, HostListener, Output, EventEmitter } from '@angular/core';

@Directive({
  selector: '[appDodragabletarget]'
})
export class DodragabletargetDirective {

  constructor() { }
  @HostListener('dragenter', ['$event'])
  onDragEnter(ev) {
    event.preventDefault();
  }
  @HostListener('dragover', ['$event'])
  onDragOver(ev) {
    event.preventDefault();
  }
  @HostListener('drop', ['$event'])
  onDrop(ev) {
    var data = ev.dataTransfer.getData("Text");  
    this.drop.next(data);
  }
  @Output('DoneDrop') drop = new EventEmitter();
}
