import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AssemblerComponent } from './assembler.component';
import { DodragabletargetDirective } from './dodragabletarget.directive';



@NgModule({
  declarations: [AssemblerComponent,DodragabletargetDirective],
  imports: [
    CommonModule
  ], exports: [AssemblerComponent]
})
export class AssemblerModule { }
