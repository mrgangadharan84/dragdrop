import { Component, OnInit, HostListener, Input } from '@angular/core';
import { CtrlDet } from '../CtrlDet';

@Component({
  selector: 'app-assembler',
  templateUrl: './assembler.component.html',
  styleUrls: ['./assembler.component.css']
})
export class AssemblerComponent implements OnInit {

  constructor() { }
  @Input() CtlsDet: Array<CtrlDet>;
  ngOnInit(): void {
  }
  Delete(index) {
    this.CtlsDet.splice(index, 1);
  }
  AddCtl(ev) {
     if (['textbox', 'dropdown', 'checkbox'].includes(ev)) {
   
      let a = new CtrlDet();
      a.CtrlType = ev;
      a.CtrlName = this.CreateName(ev);
      this.CtlsDet.reverse();
      this.CtlsDet.push(a);
      this.CtlsDet.reverse();
    }
  }
  CreateName(ev: any): string {
    let newname = "", i = 1;
    switch (ev) {
      case "textbox": newname = "Text Box "; break;
      case "dropdown": newname = "Drop Down "; break;
      case "checkbox": newname = "Checkbox "; break;
    }

         
    while(this.CtlsDet.filter(x=>x.CtrlName == (newname + i.toString())).length>0){ i++;}
      
    return newname + i;
  }
}
