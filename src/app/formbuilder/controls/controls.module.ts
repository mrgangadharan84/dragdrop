import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ControlsComponent } from './controls.component';
import { DodragableDirective } from './dodragable.directive';



@NgModule({
  declarations: [ControlsComponent,DodragableDirective],
  imports: [
    CommonModule
  ],exports:[ControlsComponent]
})
export class ControlsModule { }
