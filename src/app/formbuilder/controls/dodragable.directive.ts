import { Directive, HostBinding, Input, HostListener } from '@angular/core';

@Directive({
  selector: '[appDodragable]'
})
export class DodragableDirective {
  data: any;

  constructor() { }
  @HostBinding('draggable') get draggable() { return true; }

  @Input()
  set appDodragable(data) {
    if (data) {
      this.data = data;
    }
  }
  @HostListener('dragstart', ['$event'])
  onDragStart(ev) {
    ev.dataTransfer.effectAllowed = 'move';
    ev.dataTransfer.setData("Text", this.data);
    console.log(ev.dataTransfer.getData("Text"));
  }

}
