import { Component, OnInit, Input } from '@angular/core';
import { CtrlDet } from '../../CtrlDet';

@Component({
  selector: 'app-renderer',
  templateUrl: './renderer.component.html',
  styleUrls: ['./renderer.component.css']
})
export class RendererComponent implements OnInit {

  constructor() { }
  @Input() RenderCtlsDet: Array<CtrlDet>;
  ngOnInit(): void {
  }

}
