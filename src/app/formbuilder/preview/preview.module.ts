import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PreviewComponent } from './preview.component';

import { RendererModule } from './renderer/renderer.module';



@NgModule({
  declarations: [PreviewComponent],
  imports: [
    CommonModule,RendererModule
  ],
  exports:
  [PreviewComponent]
})
export class PreviewModule { }
