import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { CtrlDet } from '../CtrlDet';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.css']
})
export class PreviewComponent implements OnInit {
  @Input() PreviewCtlsDet: Array<CtrlDet>;
  constructor(public activeModal: NgbActiveModal) { 
  }
  ngOnInit(): void {
  }
}
